import { useState } from 'react'
import styled from 'styled-components'

import Button from '../../UI/Button/Button'
import './CourseInput.css'

const FormControl = styled.div`
  margin: 0.5rem 0;

  & label {
    font-weight: bold;
    display: block;
    margin-bottom: 0.5rem;
    color: ${({ invalid }) => (invalid ? 'lightcoral' : 'currentColor')};
  }

  & input {
    display: block;
    width: 100%;
    /* border: 1px solid #ccc; */
    /* Other way with props */
    border: 1px solid ${({ invalid }) => (invalid ? 'lightcoral' : '#ccc')};
    background-color: ${({ invalid }) =>
      invalid ? 'lightsalmon' : 'transparent'};
    font: inherit;
    line-height: 1.5rem;
    padding: 0 0.25rem;
  }

  & input:focus {
    outline: none;
    background: #fad0ec;
    border-color: #8b005d;
  }
  /* classic way */
  /* &.invalid-form input {
    border-color: lightcoral;
    background-color: lightsalmon;
  } */
  /* classic way  */
  /* &.invalid-form label {
    color: lightcoral;
  } */
`

const CourseInput = (props) => {
  const [enteredValue, setEnteredValue] = useState('')
  const [isValid, setIsValid] = useState(true)

  const goalInputChangeHandler = (event) => {
    const value = event.target.value
    if (value.trim().length > 1) {
      setIsValid(true)
    }
    setEnteredValue(value)
  }

  const formSubmitHandler = (event) => {
    event.preventDefault()
    if (enteredValue.trim().length === 0) {
      setIsValid(false)
      return
    }
    props.onAddGoal(enteredValue)
    setIsValid(true)
  }

  return (
    <form onSubmit={formSubmitHandler}>
      {/* One way of using invalid on FormControl styled component */}
      {/* <FormControl className={`${isValid ? '' : 'invalid-form'}`}> */}
      <FormControl invalid={!isValid}>
        <label>Course Goal</label>
        <input type="text" onChange={goalInputChangeHandler} />
      </FormControl>
      <Button type="submit">Add Goal</Button>
    </form>
  )
}

export default CourseInput
